# Septembre ou octobre

- Monter le slack avec tous les étudiants infos
- Planifier en décembre la demis journée à la BGE de présentation des projets
- Planifier en janvier la demis journée de pitch devant les étudiants d'info et GEA
- Planifier en janvier un atelier GEA / porteurs
- planifier la semaine agile dans l'agenda des S4

# Décembre

- Demis journée de présentation des projets 
- Jury de sélection des projets
- Annonce du planning aux porteurs

# Janvier

- Constituer les équipes
- demis journée de pitch devant les étudiants
- répartition porteurs / équipes
- atelier GEA porteurs
- Initiation au vocabualire agile
- laisser les porteurs travailler le backlog pour le retravailler en mars juste avec la semaine
- réserver le hall ou le centre de resources
- réserver les salles 
  * amphi 30 min pour l'ouverture
  * salle de DS pour la rétro
  * salles de TP avec android
  * salle de soutenance pour les porteurs
- prévenir banquiers et anciens porteurs pour constituer les jurys
- prévenir la presse pour mars

# Mars

- rappel de la presse
- rappel des membres du jury pour les porteurs
- préparer le matériel : 
  * magic chart
  * feutres
  * post-it
  * support pour les affiches
  
